﻿namespace GameControl
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OKButton = new System.Windows.Forms.Button();
            this.ChangePasswordButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.BlockDisp = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AutoStart = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // OKButton
            // 
            this.OKButton.Location = new System.Drawing.Point(197, 226);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(75, 23);
            this.OKButton.TabIndex = 0;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // ChangePasswordButton
            // 
            this.ChangePasswordButton.Location = new System.Drawing.Point(13, 13);
            this.ChangePasswordButton.Name = "ChangePasswordButton";
            this.ChangePasswordButton.Size = new System.Drawing.Size(98, 23);
            this.ChangePasswordButton.TabIndex = 1;
            this.ChangePasswordButton.Text = "Сменить пароль";
            this.ChangePasswordButton.UseVisualStyleBackColor = true;
            this.ChangePasswordButton.Click += new System.EventHandler(this.ChangePasswordButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Блокувати диспетчер";
            // 
            // BlockDisp
            // 
            this.BlockDisp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BlockDisp.FormattingEnabled = true;
            this.BlockDisp.Items.AddRange(new object[] {
            "Так (рекомендовано)",
            "Нi"});
            this.BlockDisp.Location = new System.Drawing.Point(135, 40);
            this.BlockDisp.Name = "BlockDisp";
            this.BlockDisp.Size = new System.Drawing.Size(137, 21);
            this.BlockDisp.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Включить автозапуск";
            // 
            // AutoStart
            // 
            this.AutoStart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AutoStart.FormattingEnabled = true;
            this.AutoStart.Items.AddRange(new object[] {
            "Так (рекомендовано)",
            "Нi"});
            this.AutoStart.Location = new System.Drawing.Point(135, 68);
            this.AutoStart.Name = "AutoStart";
            this.AutoStart.Size = new System.Drawing.Size(137, 21);
            this.AutoStart.TabIndex = 5;
            // 
            // Settings
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.AutoStart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BlockDisp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ChangePasswordButton);
            this.Controls.Add(this.OKButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button ChangePasswordButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox BlockDisp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox AutoStart;
    }
}