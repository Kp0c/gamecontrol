﻿namespace GameControl
{
    partial class GameControl
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameControl));
            this.ProgramList = new System.Windows.Forms.CheckedListBox();
            this.AddProgramToList = new System.Windows.Forms.Button();
            this.CloseAllChecked = new System.Windows.Forms.Button();
            this.NameLabel = new System.Windows.Forms.Label();
            this.CheckStateLabel = new System.Windows.Forms.Label();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.EverySecond = new System.Windows.Forms.Timer(this.components);
            this.WorkingTimeLabel = new System.Windows.Forms.Label();
            this.MaxWorkingTime = new System.Windows.Forms.Label();
            this.MaxHoursTextBox = new System.Windows.Forms.TextBox();
            this.HoursLabel = new System.Windows.Forms.Label();
            this.MinutesLabel = new System.Windows.Forms.Label();
            this.MaxMinutesTextBox = new System.Windows.Forms.TextBox();
            this.SecondsLabel = new System.Windows.Forms.Label();
            this.MaxSecondsTextBox = new System.Windows.Forms.TextBox();
            this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.AgreeMaxTime = new System.Windows.Forms.Button();
            this.Status = new System.Windows.Forms.StatusStrip();
            this.isWorkingLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToggleWorkingButton = new System.Windows.Forms.Button();
            this.SettingsButton = new System.Windows.Forms.Button();
            this.AutoSave = new System.Windows.Forms.Timer(this.components);
            this.Status.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProgramList
            // 
            this.ProgramList.FormattingEnabled = true;
            this.ProgramList.Location = new System.Drawing.Point(12, 12);
            this.ProgramList.Name = "ProgramList";
            this.ProgramList.Size = new System.Drawing.Size(262, 214);
            this.ProgramList.TabIndex = 0;
            this.ProgramList.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ProgramList_ItemCheck);
            this.ProgramList.SelectedIndexChanged += new System.EventHandler(this.ProgramList_SelectedIndexChanged);
            // 
            // AddProgramToList
            // 
            this.AddProgramToList.Location = new System.Drawing.Point(281, 13);
            this.AddProgramToList.Name = "AddProgramToList";
            this.AddProgramToList.Size = new System.Drawing.Size(75, 23);
            this.AddProgramToList.TabIndex = 1;
            this.AddProgramToList.Text = "Додати...";
            this.AddProgramToList.UseVisualStyleBackColor = true;
            this.AddProgramToList.Click += new System.EventHandler(this.AddProgram_Click);
            // 
            // CloseAllChecked
            // 
            this.CloseAllChecked.Location = new System.Drawing.Point(13, 233);
            this.CloseAllChecked.Name = "CloseAllChecked";
            this.CloseAllChecked.Size = new System.Drawing.Size(261, 23);
            this.CloseAllChecked.TabIndex = 2;
            this.CloseAllChecked.Text = "Закрыть все выделенное";
            this.CloseAllChecked.UseVisualStyleBackColor = true;
            this.CloseAllChecked.Click += new System.EventHandler(this.CloseAllChecked_Click);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(13, 263);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(32, 13);
            this.NameLabel.TabIndex = 3;
            this.NameLabel.Text = "Имя:";
            // 
            // CheckStateLabel
            // 
            this.CheckStateLabel.AutoSize = true;
            this.CheckStateLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CheckStateLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CheckStateLabel.Location = new System.Drawing.Point(13, 276);
            this.CheckStateLabel.Name = "CheckStateLabel";
            this.CheckStateLabel.Size = new System.Drawing.Size(58, 13);
            this.CheckStateLabel.TabIndex = 4;
            this.CheckStateLabel.Text = "Выделен?";
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(281, 43);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteButton.TabIndex = 5;
            this.DeleteButton.Text = "Видалити";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // EverySecond
            // 
            this.EverySecond.Enabled = true;
            this.EverySecond.Interval = 1000;
            this.EverySecond.Tick += new System.EventHandler(this.EverySecond_Tick);
            // 
            // WorkingTimeLabel
            // 
            this.WorkingTimeLabel.AutoSize = true;
            this.WorkingTimeLabel.Location = new System.Drawing.Point(13, 289);
            this.WorkingTimeLabel.Name = "WorkingTimeLabel";
            this.WorkingTimeLabel.Size = new System.Drawing.Size(83, 13);
            this.WorkingTimeLabel.TabIndex = 6;
            this.WorkingTimeLabel.Text = "Время работы:";
            // 
            // MaxWorkingTime
            // 
            this.MaxWorkingTime.AutoSize = true;
            this.MaxWorkingTime.Location = new System.Drawing.Point(13, 302);
            this.MaxWorkingTime.Name = "MaxWorkingTime";
            this.MaxWorkingTime.Size = new System.Drawing.Size(162, 13);
            this.MaxWorkingTime.TabIndex = 7;
            this.MaxWorkingTime.Text = "Максимальное время работы:";
            // 
            // MaxHoursTextBox
            // 
            this.MaxHoursTextBox.Location = new System.Drawing.Point(64, 316);
            this.MaxHoursTextBox.Name = "MaxHoursTextBox";
            this.MaxHoursTextBox.Size = new System.Drawing.Size(100, 20);
            this.MaxHoursTextBox.TabIndex = 8;
            // 
            // HoursLabel
            // 
            this.HoursLabel.AutoSize = true;
            this.HoursLabel.Location = new System.Drawing.Point(16, 319);
            this.HoursLabel.Name = "HoursLabel";
            this.HoursLabel.Size = new System.Drawing.Size(42, 13);
            this.HoursLabel.TabIndex = 9;
            this.HoursLabel.Text = "Часов:";
            // 
            // MinutesLabel
            // 
            this.MinutesLabel.AutoSize = true;
            this.MinutesLabel.Location = new System.Drawing.Point(16, 345);
            this.MinutesLabel.Name = "MinutesLabel";
            this.MinutesLabel.Size = new System.Drawing.Size(41, 13);
            this.MinutesLabel.TabIndex = 11;
            this.MinutesLabel.Text = "Минут:";
            // 
            // MaxMinutesTextBox
            // 
            this.MaxMinutesTextBox.Location = new System.Drawing.Point(64, 342);
            this.MaxMinutesTextBox.Name = "MaxMinutesTextBox";
            this.MaxMinutesTextBox.Size = new System.Drawing.Size(100, 20);
            this.MaxMinutesTextBox.TabIndex = 10;
            // 
            // SecondsLabel
            // 
            this.SecondsLabel.AutoSize = true;
            this.SecondsLabel.Location = new System.Drawing.Point(16, 371);
            this.SecondsLabel.Name = "SecondsLabel";
            this.SecondsLabel.Size = new System.Drawing.Size(46, 13);
            this.SecondsLabel.TabIndex = 13;
            this.SecondsLabel.Text = "Секунд:";
            // 
            // MaxSecondsTextBox
            // 
            this.MaxSecondsTextBox.Location = new System.Drawing.Point(64, 368);
            this.MaxSecondsTextBox.Name = "MaxSecondsTextBox";
            this.MaxSecondsTextBox.Size = new System.Drawing.Size(100, 20);
            this.MaxSecondsTextBox.TabIndex = 12;
            // 
            // TrayIcon
            // 
            this.TrayIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Warning;
            this.TrayIcon.BalloonTipText = "text";
            this.TrayIcon.BalloonTipTitle = "titlr";
            this.TrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayIcon.Icon")));
            this.TrayIcon.Text = "GameControl";
            this.TrayIcon.Visible = true;
            this.TrayIcon.Click += new System.EventHandler(this.TrayIcon_Click);
            // 
            // AgreeMaxTime
            // 
            this.AgreeMaxTime.Location = new System.Drawing.Point(126, 395);
            this.AgreeMaxTime.Name = "AgreeMaxTime";
            this.AgreeMaxTime.Size = new System.Drawing.Size(37, 23);
            this.AgreeMaxTime.TabIndex = 14;
            this.AgreeMaxTime.Text = "OK";
            this.AgreeMaxTime.UseVisualStyleBackColor = true;
            this.AgreeMaxTime.Click += new System.EventHandler(this.AgreeMaxTime_Click);
            // 
            // Status
            // 
            this.Status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.isWorkingLabel});
            this.Status.Location = new System.Drawing.Point(0, 445);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(490, 22);
            this.Status.TabIndex = 15;
            this.Status.Text = "statusStrip1";
            // 
            // isWorkingLabel
            // 
            this.isWorkingLabel.ForeColor = System.Drawing.Color.Red;
            this.isWorkingLabel.Name = "isWorkingLabel";
            this.isWorkingLabel.Size = new System.Drawing.Size(140, 17);
            this.isWorkingLabel.Text = "Программа не работает";
            // 
            // ToggleWorkingButton
            // 
            this.ToggleWorkingButton.Location = new System.Drawing.Point(403, 419);
            this.ToggleWorkingButton.Name = "ToggleWorkingButton";
            this.ToggleWorkingButton.Size = new System.Drawing.Size(75, 23);
            this.ToggleWorkingButton.TabIndex = 16;
            this.ToggleWorkingButton.Text = "ON";
            this.ToggleWorkingButton.UseVisualStyleBackColor = true;
            this.ToggleWorkingButton.Click += new System.EventHandler(this.ToggleWorkingButton_Click);
            // 
            // SettingsButton
            // 
            this.SettingsButton.Location = new System.Drawing.Point(403, 12);
            this.SettingsButton.Name = "SettingsButton";
            this.SettingsButton.Size = new System.Drawing.Size(75, 23);
            this.SettingsButton.TabIndex = 17;
            this.SettingsButton.Text = "Настройки";
            this.SettingsButton.UseVisualStyleBackColor = true;
            this.SettingsButton.Click += new System.EventHandler(this.SettingsButton_Click);
            // 
            // AutoSave
            // 
            this.AutoSave.Enabled = true;
            this.AutoSave.Interval = 60000;
            this.AutoSave.Tick += new System.EventHandler(this.AutoSave_Tick);
            // 
            // GameControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 467);
            this.Controls.Add(this.SettingsButton);
            this.Controls.Add(this.ToggleWorkingButton);
            this.Controls.Add(this.Status);
            this.Controls.Add(this.AgreeMaxTime);
            this.Controls.Add(this.SecondsLabel);
            this.Controls.Add(this.MaxSecondsTextBox);
            this.Controls.Add(this.MinutesLabel);
            this.Controls.Add(this.MaxMinutesTextBox);
            this.Controls.Add(this.HoursLabel);
            this.Controls.Add(this.MaxHoursTextBox);
            this.Controls.Add(this.MaxWorkingTime);
            this.Controls.Add(this.WorkingTimeLabel);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.CheckStateLabel);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.CloseAllChecked);
            this.Controls.Add(this.AddProgramToList);
            this.Controls.Add(this.ProgramList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "GameControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GameControl";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameControl_FormClosing);
            this.Resize += new System.EventHandler(this.GameControl_Resize);
            this.Status.ResumeLayout(false);
            this.Status.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddProgramToList;
        public System.Windows.Forms.CheckedListBox ProgramList;
        private System.Windows.Forms.Button CloseAllChecked;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label CheckStateLabel;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.Timer EverySecond;
        private System.Windows.Forms.Label WorkingTimeLabel;
        private System.Windows.Forms.Label MaxWorkingTime;
        private System.Windows.Forms.TextBox MaxHoursTextBox;
        private System.Windows.Forms.Label HoursLabel;
        private System.Windows.Forms.Label MinutesLabel;
        private System.Windows.Forms.TextBox MaxMinutesTextBox;
        private System.Windows.Forms.Label SecondsLabel;
        private System.Windows.Forms.TextBox MaxSecondsTextBox;
        private System.Windows.Forms.NotifyIcon TrayIcon;
        private System.Windows.Forms.Button AgreeMaxTime;
        private System.Windows.Forms.StatusStrip Status;
        private System.Windows.Forms.ToolStripStatusLabel isWorkingLabel;
        private System.Windows.Forms.Button ToggleWorkingButton;
        private System.Windows.Forms.Button SettingsButton;
        private System.Windows.Forms.Timer AutoSave;
    }
}

