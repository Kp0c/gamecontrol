﻿using System;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;

namespace GameControl
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void ChangePasswordButton_Click(object sender, EventArgs e)
        {
            ChangePassword change = new ChangePassword();
            change.ShowDialog();
        }

        private void Settings_Load(object sender, EventArgs e)
        {
            BlockDisp.SelectedIndex = 0;
            AutoStart.SelectedIndex = 0;
        }

        private void OKButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (AutoStart.SelectedIndex == 0) SetAutorunValue(true); else SetAutorunValue(false);
                if (BlockDisp.SelectedIndex == 0) SetValue("BlockDisp", "true"); else DeleteValue("BlockDisp");
                Log.Log4Parent(5, "Изменение настроек.");
            }
            catch (Exception ex) { Log.LogError(ex.Message); }
            this.Close();
        }

        public void SetValue(string name, string value)
        {
            Registry.CurrentUser.CreateSubKey("GC\\").SetValue(name,value);
        }

        public void DeleteValue(string name)
        {
            Registry.CurrentUser.CreateSubKey("GC\\").DeleteValue(name);
        }

        public void SetAutorunValue(bool autorun)
        {
            string name = "GameControl";
            string exePath = System.Windows.Forms.Application.ExecutablePath+" minimized on";
            RegistryKey reg;
            reg = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\");
            try
            {
                if (autorun)
                {
                    reg.SetValue(name, exePath);
                }
                else
                {
                    reg.DeleteValue(name);
                }
                reg.Close();
            }
            catch(Exception ex)
            {
                Log.LogError(ex.Message);
            }
        }
    }
}
