using System;
using System.Windows.Forms;

namespace GameControl
{
    [Serializable]
    public class Item
    {
        public string Name;
        public CheckState CheckState = CheckState.Unchecked;
        public TimeSpan WorkingTime = new TimeSpan(0,0,0);
        public int MaxTimeHours;
        public int MaxTimeMinutes;
        public int MaxTimeSeconds;

        public Item(string name)
        {
            this.Name = name;
        }
    }
}