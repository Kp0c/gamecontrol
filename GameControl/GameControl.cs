﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Win32;

namespace GameControl
{
    public partial class GameControl : Form
    {
        Process[] temp;
        bool isWorking = false;
        public string LastEnteredPass = "";
        List<Item> items = new List<Item>();
        bool minimized = false;
        bool on = false;

        public GameControl(bool minimized, bool on)
        {
            InitializeComponent();
            this.minimized = minimized;
            this.on = on;
            if (minimized)
            {
                this.ShowInTaskbar = false;
                this.WindowState = FormWindowState.Minimized;
            }
            if (on) ToggleWorkingButton_Click(this, new EventArgs());
            Log.Init();
            Log.Log4Parent(1,"Инициализирование программы.");
            LoadItems();

        }

        public void AddItem(string name)
        {
            if (name == "")
            {
                MessageBox.Show(Lang.Get("Error") + " - Имя не может быть пустое");
                Log.LogError("Имя не может быть пустое");
            }
            else
            if (!ProgramList.Items.Contains(name))
            {
                items.Add(new Item(name));
                ProgramList.Items.Add(name);
            }
            else
            {
                MessageBox.Show(Lang.Get("Error") + " - " + name + " " + Lang.Get("AlreadyExistInList"));
                Log.LogError(name + " " + Lang.Get("AlreadyExistInList"));
            }
        }

        private void AddProgram_Click(object sender, EventArgs e)
        {
            AddProgram addProgram = new AddProgram(this);
            addProgram.ShowDialog(this);
        }

        private void CloseAllChecked_Click(object sender, EventArgs e)
        {
            temp = Process.GetProcesses();
            foreach (string process in ProgramList.CheckedItems)
            {
                foreach (Process temp2 in temp)
                {
                    if (temp2.ProcessName.ToLower().Contains(process.ToLower()))
                    {
                        temp2.Kill();
                        break;
                    }
                }
            }
            TrayIcon.ShowBalloonTip(5000, "Закрытие программ", "Закрытие всех программ", ToolTipIcon.Info);
        }

        private void RefreshState()
        {
            try
            {
                Item selectedItem =
                    items.Find(mc => mc.Name == ProgramList.SelectedItem.ToString());
                NameLabel.Text = Lang.Get("Name") + ": " + selectedItem.Name;
                CheckStateLabel.Text = Lang.Get("CheckState") + ": " + selectedItem.CheckState.ToString();

                WorkingTimeLabel.Text = Lang.Get("WorkingTime") + ": " + selectedItem.WorkingTime.ToString();
            }
            catch (NullReferenceException) { }
            catch (Exception ex) { Log.LogError(ex.Message); }
        }

        private void RefreshMaxTime()
        {
            try
            {
                Item selectedItem =
                    items.Find(mc => mc.Name == ProgramList.SelectedItem.ToString());
                MaxHoursTextBox.Text = selectedItem.MaxTimeHours.ToString();
                MaxMinutesTextBox.Text = selectedItem.MaxTimeMinutes.ToString();
                MaxSecondsTextBox.Text = selectedItem.MaxTimeSeconds.ToString();
            }
            catch (Exception ex) { Log.LogError(ex.Message); }
        }

        private bool CheckPassword()
        {
            Form checkPass = new CheckPassword(this);
            checkPass.ShowDialog(this);
            if (LastEnteredPass != Registry.GetValue("HKEY_CURRENT_USER\\GC","password","1111").ToString())
            {
                MessageBox.Show("Введений пароль " + LastEnteredPass + " неправильний, перевiрте його", "Error");
                return false;
            }
            else return true;
        }

        private void ProgramList_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshState();
            RefreshMaxTime();
        }

        private void SaveItems()
        {
            if (!Directory.Exists(Application.StartupPath + "\\Items")) 
                Directory.CreateDirectory(Application.StartupPath + "\\Items");
            else
            {
                foreach (var item in Directory.GetFiles(Application.StartupPath+"\\Items"))
                {
                    File.Delete(item);
                }
            }
            BinaryFormatter serializer = new BinaryFormatter();
            foreach (Item item in items)
            {
                Stream saveStream = File.Create(Application.StartupPath + "\\Items\\" + item.Name + ".item");
                serializer.Serialize(saveStream, item);
                saveStream.Close();
            }
        }

        private void GameControl_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveItems();
            if (e.CloseReason != CloseReason.WindowsShutDown)
            {
                if (CheckPassword())
                {
                    Log.Log4Parent(1, "Выключение программы по паролю");
                    Log.SW.Close();
                    Log.SW2.Close();
                }
                else e.Cancel = true;
            }
            else
            {
                Log.Log4Parent(1, "Выключение компьютера.");
                Log.SW.Close();
                Log.SW2.Close();
            }

        }

        private void LoadItems()
        {
            if (Directory.Exists(Application.StartupPath + "\\Items"))
            {
                DirectoryInfo dir = new DirectoryInfo(Application.StartupPath +"\\Items");
                BinaryFormatter deserializer = new BinaryFormatter();
                Item tmp;
                foreach (var item in dir.GetFiles())
                {
                    Stream loadStream = item.OpenRead();
                    try
                    {
                        tmp = (Item)deserializer.Deserialize(loadStream);
                        items.Add(tmp);
                        ProgramList.Items.Add(tmp.Name, tmp.CheckState);
                        //loadStream.Close();
                    }
                    catch (Exception ex)
                    {
                       Log.LogError(ex.Message);
                    }
                    finally
                    {
                        loadStream.Close();
                    }
                }
            }
        }

        private void ProgramList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            try
            {
                items.Find(mc => mc.Name == ProgramList.SelectedItem.ToString()).CheckState = e.NewValue;
                RefreshState();
            }
            catch (Exception ex) { Log.LogError(ex.Message); }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            items.Remove(items.Find(mc => mc.Name == ProgramList.SelectedItem.ToString()));
            ProgramList.Items.Remove(ProgramList.SelectedItem);
        }

        private void EverySecond_Tick(object sender, EventArgs e)
        {
            temp = Process.GetProcesses();
            if (isWorking)
            {
                
                foreach (string process in ProgramList.CheckedItems)
                {
                    bool onceMessage = true;
                    bool onceAddTime = true;
                    Item item = items.Find(mc => mc.Name == process);
                    foreach (Process temp2 in temp)
                    {
                        #region Kill process
                        if (temp2.ProcessName.ToLower().Contains(process.ToLower()))
                        {
                            if (onceAddTime)
                            {
                                item.WorkingTime = item.WorkingTime.Add(new TimeSpan(0, 0, 1));
                                onceAddTime = false;
                            }
                            if ((item.MaxTimeHours != 0 || item.MaxTimeMinutes != 0 || item.MaxTimeSeconds != 0)
                               && new TimeSpan(item.MaxTimeHours, item.MaxTimeMinutes, item.MaxTimeSeconds).Subtract
                            (item.WorkingTime) <= new TimeSpan(0, 0, 0))
                            {
                                try
                                {
                                    temp2.Kill();
                                    if (onceMessage)
                                    {
                                        TrayIcon.ShowBalloonTip(5000, "Закончилось время", "Время работы программы " +
                                        temp2.ProcessName + " истекло", ToolTipIcon.Info);
                                        onceMessage = false;
                                    }
                                }
                                catch (Exception ex) { Log.LogError(ex.Message); }
                            }
                        }
                        #endregion

                        try
                        {
                            if (temp2.ProcessName.ToLower() == "taskmgr" && Convert.ToBoolean
                                (Registry.GetValue("HKEY_CURRENT_USER\\GC", "BlockDisp", false).ToString()))
                                temp2.Kill();
                        }
                        catch (Exception) { }
                    }
                }
               
            }
            RefreshState();
        }

        private void TrayIcon_Click(object sender, EventArgs e)
        {
            if (CheckPassword())
            {
                this.ShowInTaskbar = true;
                this.Show();
                this.WindowState = FormWindowState.Normal;
                TrayIcon.Visible = false;
            }
            else
            {
                TrayIcon.ShowBalloonTip(2500, "Неправильный пароль", @"Вы ввели неправильный пароль,
                данные записаны в лог родительского контроля", ToolTipIcon.Error);
            }
        }

        private void GameControl_Resize(object sender, EventArgs e)
        {
                if (this.WindowState == FormWindowState.Minimized)
                {
                    this.ShowInTaskbar = false;
                    this.Hide();
                    TrayIcon.Visible = true;
                }
        }

        private void AgreeMaxTime_Click(object sender, EventArgs e)
        {
            try
            {
                items.Find(mc => mc.Name == ProgramList.SelectedItem.ToString()).MaxTimeHours
                    = Convert.ToInt32(MaxHoursTextBox.Text);
                items.Find(mc => mc.Name == ProgramList.SelectedItem.ToString()).MaxTimeMinutes
                    = Convert.ToInt32(MaxMinutesTextBox.Text);
                items.Find(mc => mc.Name == ProgramList.SelectedItem.ToString()).MaxTimeSeconds
                    = Convert.ToInt32(MaxSecondsTextBox.Text);
                RefreshMaxTime();
            }
            catch (Exception ex) { Log.LogError(ex.Message); }
        }

        private void ToggleWorkingButton_Click(object sender, EventArgs e)
        {
            if (isWorking)
            {
                ToggleWorkingButton.Text = "ON";
                isWorkingLabel.Text = "Программа не работает";
                isWorkingLabel.ForeColor = Color.Red;
            }
            else
            {
                ToggleWorkingButton.Text = "OFF";
                isWorkingLabel.Text = "Программа работает";
                isWorkingLabel.ForeColor = Color.Green;
            }
            isWorking = !isWorking;
        }

        private void SettingsButton_Click(object sender, EventArgs e)
        {
            if (CheckPassword())
            {
                Settings settingsForm = new Settings();
                settingsForm.ShowDialog(this);
            }
        }

        private void AutoSave_Tick(object sender, EventArgs e)
        {
            SaveItems();
        }
    }

}