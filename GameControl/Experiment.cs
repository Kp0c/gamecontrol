﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace GameControl
{
    public partial class Experiment : Component
    {
        public Experiment()
        {
            InitializeComponent();
        }

        public Experiment(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
    }
}
