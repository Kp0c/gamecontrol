﻿using System;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;

namespace GameControl
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == textBox2.Text)
            {
                if (textBox1.Text != "" && textBox2.Text != "")
                {
                    Registry.CurrentUser.CreateSubKey("GC\\").SetValue("password", textBox1.Text);
                    Log.Log4Parent(5, "Изменение пароля!");
                    this.Close();
                }
                else MessageBox.Show("Пароль не может быть пустым");
            }
            else MessageBox.Show("Пароли не совпадают");
        }
    }
}
