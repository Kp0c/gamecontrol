﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace GameControl
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            bool minimized = false;
            bool on = false;
            foreach (string arg in args)
            {
                if (arg == "minimized") minimized = true;
                if (arg == "on") on = true;
            }
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new GameControl(minimized,on));
        }
    }
}
