﻿using System;
using System.Linq;
using System.Resources;
using System.Reflection;

namespace GameControl
{
    sealed class Lang
    {
        private static readonly ResourceManager resourceManager = new ResourceManager("GameControl.Properties.GameControl", Assembly.GetCallingAssembly());

        public static string Get(string name)
        {
            return resourceManager.GetString(name);
        }
    }
}
