﻿using System;
using System.Linq;
using System.IO;
using System.Windows.Forms;

namespace GameControl
{
    static class Log
    {
        public static StreamWriter SW = null;
        public static StreamWriter SW2 = null;

        public static void Init()
        {
            while (SW == null)
            {
                try
                {
                    SW = new StreamWriter(Application.StartupPath+"\\ErrorLog.txt",true);
                }
                catch (Exception) { }
            }

            while (SW2 == null)
            {
                try
                {
                    SW2 = new StreamWriter(Application.StartupPath + "\\ParentLog.txt", true);
                }
                catch (Exception) { }
            }
        }

        public static void LogError(string error)
        {
            try
            {
                string date = "[" + DateTime.Today.Day + "." + DateTime.Today.Month + "." + DateTime.Today.Year + "] ";
                string time = DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "\t";
                SW.WriteLine(date + time + "[" + Lang.Get("Error").ToUpper() + "]" + error);
            }
            catch (Exception) { }
        }

        public static void Log4Parent(int lvl,string log)
        {
            try
            {
                string time = DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "\t";
                SW2.WriteLine(time + "[LEVEL: " + lvl + "]\t" + log);
            }
            catch (Exception) { }
        }
    }
}
