﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;

namespace GameControl
{
    public partial class AddProgram : Form
    {
        bool isClose = false;
        GameControl parent;

        public AddProgram(GameControl parent)
        {
            InitializeComponent();
            TopMost = true;
            this.parent = parent;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                parent.AddItem(NameProcess.Text);
                isClose = true;
                this.Close();
            }
            if (tabControl1.SelectedIndex == 1)
            {
                parent.AddItem(ProcessBox.SelectedItem.ToString().Split(" ".ToCharArray())[0]);
                isClose = true;
                this.Close();
            }
        }

        private void AddProgram_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !isClose;
        }

        private void AddProgram_Load(object sender, EventArgs e)
        {
            Process[] process = Process.GetProcesses();
            foreach (Process proc in process)
            {
                try
                {
                    ProcessBox.Items.Add(proc.ProcessName + " - " + proc.MainModule.FileVersionInfo.FileDescription);
                }
                catch (Exception)
                {
                    ProcessBox.Items.Add(proc.ProcessName + " - без описания");
                }
            }
        }
    }
}
