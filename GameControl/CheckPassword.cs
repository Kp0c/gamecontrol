﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace GameControl
{
    public partial class CheckPassword : Form
    {
        GameControl parent;

        public CheckPassword(GameControl parent)
        {
            InitializeComponent();
            this.Activate();
            this.parent = parent;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            parent.LastEnteredPass = Password.Text;
        }
    }
}
